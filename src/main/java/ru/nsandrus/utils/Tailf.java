package ru.nsandrus.utils;

import java.io.IOException;

public class Tailf {

	final static String VERSION = "1.5.1.0";

	public static void main(String[] args) {
		cls();
		System.out.println("version " + VERSION);
		ReaderLog log1 = new ReaderLog();
		try {
			log1.run(args);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void cls(String... arg) {
		try {
			new ProcessBuilder("cmd", "/c", "cls").inheritIO().start().waitFor();
			return;
		} catch (InterruptedException | IOException e) {
			System.out.println("clear screen not working here");
		}
	}

}