package ru.nsandrus.utils;

import java.util.EnumMap;

public class Message {

	private static final String BEGIN_GF_LOG = "[#";
	private static final int NUM_COLUMNS_GLASSFISH_LOG = 8;
	private static final String DELIMETER_GF = "[|]";

	public enum COLUMN{ START, TIME, LEVEL, SERVER, ENGINE, THREAD, DATA, END};
	/*
	* [#|2015-10-23T20:08:00.057+0300|INFO|sun-appserver2.1|com.sun.jbi.engine.bpel.core.bpel.trace.BPELTraceManager|_ThreadID=430;
	* _ThreadName=sun-bpel-engine-thread-28;Process Instance Id=192.168.168.113:67efc9fa:15095a78842:-7a04;Service Assembly Name=SMSFromFileCA;Activity Name=ReadFile;
	* BPEL Process Name=SMBDirect;|Invoke::File::Start::SessionID=192.168.168.113:67efc9fa:15095a78842:-7a0b::Business data::Path=\\ms-glass006\temp\|#]
	*/
	private EnumMap<COLUMN, String> map;
	
	public Message(String readText) {
		map = new EnumMap<>(COLUMN.class);
		fillMap(readText);
	}

	private void fillMap(String readMessage) {
		String[] split = readMessage.split(DELIMETER_GF);
		if (isGlassFishLog(split)) {
			fillGlassfishLog(split);
		} else {
			fillOtherLog(readMessage);
		}
	}

	protected boolean isGlassFishLog(String[] split) {
		return split.length == NUM_COLUMNS_GLASSFISH_LOG && split[0].contains(BEGIN_GF_LOG);
	}

	private void fillOtherLog(String readText) {
		fillEmptyMap();
		map.put(COLUMN.DATA, readText);
		
	}

	private void fillEmptyMap() {
		for (COLUMN colEnum : COLUMN.values()) {
			map.put(colEnum, "");
		}
	}

	private void fillGlassfishLog(String[] split) {
		for (COLUMN colEnum : COLUMN.values()) {
			map.put(colEnum, split[colEnum.ordinal()]);
		}
	}
	
	public EnumMap<COLUMN, String> getMessageMap() {
		return map;
	}
	
	protected String deleteLastDelimeter(String result) {
		if (result.length()>0) {
			result = result.substring(0, result.length()-1);
		}
		return result;
	}
	
	@Override
	public String toString() {
		String result="";
		for (COLUMN colEnum : COLUMN.values()) {
			String col = map.get(colEnum);
			if (!col.isEmpty()) {
				result += col + '|';
			}
		}
	result = deleteLastDelimeter(result);
	return result;
	}
	
}
