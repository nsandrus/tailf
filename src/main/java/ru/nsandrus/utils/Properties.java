package ru.nsandrus.utils;

import java.util.ArrayList;

public class Properties {

	public static final String SIMBOL_WORDS = "%";
	
	private boolean isGrepSet;
	private boolean isSilent;
	private boolean isLight;
	private int workTime;
	private int numPostPrint;
	private int maxLenMessage = 10_000_000;
	private String configFilename = "";
	private String filenameLogfile = "";
	private String[] arguments;

	private ArrayList<String> exceptWords = new ArrayList<String>();
	private ArrayList<String> searchWords = new ArrayList<String>();
	private ArrayList<String> necessariWords = new ArrayList<String>();

	private String charset;

	public String toString() {
		
		return "Chunk simbol=" + SIMBOL_WORDS +
				"::grep=" + isGrepSet +
				"::silent=" + isSilent +
				"::time=" + workTime +
				"::A=" + numPostPrint +
				"::maxout=" + maxLenMessage +
				"::cfg=" + configFilename +
				"::log=" + filenameLogfile +
				"::charset=" + charset +
				"::NOT=" + exceptWords +
				"::OR=" + searchWords +
				"::AND" + necessariWords;
				
		
	}
	
	public boolean isGrepSet() {
		return isGrepSet;
	}

	public void setGrepSet(boolean isGrepSet) {
		this.isGrepSet = isGrepSet;
	}

	public boolean isSilent() {
		return isSilent;
	}

	public void setSilent(boolean isSilent) {
		this.isSilent = isSilent;
	}

	public int getWorkTime() {
		return workTime;
	}

	public void setWorkTime(int workTime) {
		this.workTime = workTime;
	}

	public int getNumPostPrint() {
		return numPostPrint;
	}

	public void setNumPostPrint(int numPostPrint) {
		this.numPostPrint = numPostPrint;
	}

	public int getMaxLenMessage() {
		return maxLenMessage;
	}

	public void setMaxLenMessage(int maxLenMessage) {
		this.maxLenMessage = maxLenMessage;
	}

	public String getConfigFilename() {
		return configFilename;
	}

	public void setConfigFilename(String configFilename) {
		this.configFilename = configFilename;
	}

	public String[] getArguments() {
		return arguments;
	}

	public void setArguments(String[] arguments) {
		this.arguments = arguments;
	}

	public String getFilenameLogfile() {
		return filenameLogfile;
	}

	public void setFilenameLogfile(String filenameLogfile) {
		this.filenameLogfile = filenameLogfile;
	}

	public ArrayList<String> getExceptWords() {
		return exceptWords;
	}

	public void addExceptWords(String exceptWords) {
		this.exceptWords.add(exceptWords);
	}

	public ArrayList<String> getSearchWords() {
		return searchWords;
	}

	public void addSearchWords(String searchWords) {
		this.searchWords.add(searchWords);
	}

	public ArrayList<String> getNecessariWords() {
		return necessariWords;
	}

	public void addNecessariWords(String necessariWords) {
		this.necessariWords.add(necessariWords);
	}

	public boolean isLight() {
		return isLight;
	}

	public void setLight(boolean isLight) {
		this.isLight = isLight;
	}

	public void setCharset(String charset) {
		this.charset= charset;
	}

	public String getCharset() {
		return charset;
	}

}
