package ru.nsandrus.utils;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

public class MessageColor {
	final static Logger LOG = Logger.getLogger(MessageColor.class);

	private HashMap<Integer, String> levels = new HashMap<>();
	private ArrayList<Integer> colorSearchEnd = new ArrayList<>();
	private static final char SPEC = (char) 27;

	private static final String GREY = SPEC + "[0;37m";
	private static final String RED = SPEC + "[1;31m";
	private static final String GREEN = SPEC + "[0;32m";
	// private static final String GREEN_LIGHT = SPEC + "[1;32m";
	private static final String WHITE = SPEC + "[1;37m";
	private static final String YELLOW = SPEC + "[1;33m";
	private static final String CYAN = SPEC + "[1;36m";
	private static final String BLUE = SPEC + "[0;34m";

	private ArrayList<String> colorColumns;

	public MessageColor(String col1, String col2, String col3, String col4, String col5, String col6, String col7,
			String col8, String brace, String quotes, String find, String assemblyName, String processId) {
		colorColumns = new ArrayList<String>();
		colorColumns.add(0, col1);
		colorColumns.add(1, col2);
		colorColumns.add(2, col3);
		colorColumns.add(3, col4);
		colorColumns.add(4, col5);
		colorColumns.add(5, col6);
		colorColumns.add(6, col7);
		colorColumns.add(7, col8);
		colorColumns.add(8, brace);
		colorColumns.add(9, quotes);
		colorColumns.add(10, find);
		colorColumns.add(11, assemblyName);
		colorColumns.add(12, processId);

	}

	public MessageColor() {
		this(GREY, GREY, GREY, BLUE, GREY, GREY, YELLOW, GREY, CYAN, WHITE, GREY, GREEN, WHITE);
	}

	private StringBuilder buildColorMessage(String mess) {
		setUpColorWords(mess);

		int column = 0;
		boolean qoutes = false;
		String currentColor = colorColumns.get(0);
		StringBuilder result = new StringBuilder();
		result.append(currentColor);
		int index = 0;
		for (char ch : mess.toCharArray()) {

			if (levels.containsKey(index)) {
				result.append(levels.get(index));
				result.append(ch);
			} else if (colorSearchEnd.contains(index)) {
				result.append(currentColor);
				result.append(ch);
			} else

			if (ch == '"') {
				if (qoutes) {
					qoutes = false;
					result.append(ch);
					result.append(currentColor);
				} else {
					qoutes = true;
					result.append(colorColumns.get(9));
					result.append(ch);
				}
			} else if (ch == '>') {
				result.append(ch);
				result.append(colorColumns.get(8));
			} else if (ch == '<') {
				result.append(currentColor);
				result.append(ch);
			} else if (ch == '|') {
				column++;
				currentColor = getCurrentColor(column);
				result.append(currentColor);
				result.append(ch);
			} else {
				result.append(ch);
			}
			index++;
		}

		return result;
	}

	
	private void setUpColorWords(String mess) {
		levels.clear();
		colorSearchEnd.clear();
		levels.put(mess.indexOf("INFO|"), GREEN);
		levels.put(mess.indexOf("FINE|"), GREEN);
		levels.put(mess.indexOf("FINEST|"), GREEN);
		levels.put(mess.indexOf("WARNING|"), RED);
		levels.put(mess.indexOf("ERROR|"), RED);
		levels.put(mess.indexOf("SEVERE|"), RED);
		addIndexes(mess, "Service Assembly Name=", colorColumns.get(11));
		addIndexes(mess, "Activity Name=", colorColumns.get(11));
		addIndexes(mess, "Process Instance Id=", colorColumns.get(12));
	}

	
	private void addIndexes(String mess, String word, String color) {
		int start = mess.indexOf(word);
		if (start >= 0) {
			levels.put(start + word.length(), color);
			int end = mess.indexOf(";", start);
			if (end > start) {
				colorSearchEnd.add(end);
			}
		}
	}

	
	private String getCurrentColor(int column) {
		if (column > 7) {
			column = 7;
		}
		return colorColumns.get(column);
	}

	
	void setColorScheme(String textConfig) {
		String keyWord = "";
		String value = "";
		String[] strTextConfig = textConfig.split("\\\n");
		for (String strConfig : strTextConfig) {
			keyWord = getKeyWordConfig(strConfig);
			value = getValueConfig(strConfig);
			setColorVar(keyWord, value);
		}
	}

	
	private String getKeyWordConfig(String strLine) {
		String result = "";
		int index = strLine.indexOf("=");
		if (index < 0) {
			return "";
		}
		result = strLine.substring(0, index);
		return result;

	}

	
	private String getValueConfig(String strConfig) {
		int index = strConfig.indexOf("=");
		String result = strConfig.substring(index + 1, strConfig.length() - 1);
		return SPEC + "[" + result;
	}

	
	private void setColorVar(String keyWord, String value) {
		if (keyWord.equals("col1")) {
			colorColumns.add(0, value);
		} else if (keyWord.equals("col2")) {
			colorColumns.add(1, value);
		} else if (keyWord.equals("col3")) {
			colorColumns.add(2, value);
		} else if (keyWord.equals("col4")) {
			colorColumns.add(3, value);
		} else if (keyWord.equals("col5")) {
			colorColumns.add(4, value);
		} else if (keyWord.equals("col6")) {
			colorColumns.add(5, value);
		} else if (keyWord.equals("col7")) {
			colorColumns.add(6, value);
		} else if (keyWord.equals("col8")) {
			colorColumns.add(7, value);
		} else if (keyWord.equals("brace")) {
			colorColumns.add(8, value);
		} else if (keyWord.equals("quotes")) {
			colorColumns.add(9, value);
		} else if (keyWord.equals("find")) {
			colorColumns.add(10, value);
		} else if (keyWord.equals("assemblyname")) {
			colorColumns.add(11, value);
		} else if (keyWord.equals("processId")) {
			colorColumns.add(12, value);
		}

	}

	public String getMessageColorized(String message, ArrayList<String> searchWords, boolean isLight) {
		StringBuilder sborka = buildColorMessage(message);
		String result = sborka.toString();
		return result;
	}

}