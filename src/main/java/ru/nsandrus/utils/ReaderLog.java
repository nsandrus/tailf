package ru.nsandrus.utils;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import org.apache.log4j.Logger;

import ru.nsandrus.utils.viewer.Viewer;

public class ReaderLog {
	final static Logger LOG = Logger.getLogger(ReaderLog.class);
	
	protected String filenameLogfile;
	protected Viewer viewer = new Viewer();

	
	private boolean isSilent = false;
	private int workTime = 0;
	private String charsetName = "CP1251";
	private Long endTime;
	private Properties prop;
	private Filter filter;
	private StringBuilder allResultFiltered = new StringBuilder();
	private MessageColor messageColorized = new MessageColor();
	
	Config configuration = new Config();

	public void run(String[] args) throws IOException {
		initialization(args);
		cyclesForever();
	}

	public void initialization(String[] args) throws IOException {
		if (args.length == 0) {
			printUsed();
			return;
		}
		ParsingArgs parsing = new ParsingArgs(args);
		prop = parsing.getProperties();
		printWords();
		String configFile = prop.getConfigFilename();
		setVars();
		setCharsetFile();

		if (filenameLogfile.isEmpty()) {
			LOG.fatal("Initialization::Error.Empty filename log fileread log file");
			System.out.println("Please set filename log file");
			System.exit(0);
		}

		readConfigFile(configFile);
		String configText = configuration.getConfigText();
		messageColorized.setColorScheme(configText);
		LOG.info("End::Initialization");
	}

	private void setVars() {
		filenameLogfile = prop.getFilenameLogfile();
		isSilent = prop.isSilent();
		workTime = prop.getWorkTime();
		filter = new Filter(prop);
	}

	private void setCharsetFile() {
		if (filenameLogfile.contains("karaf.log")) {
			charsetName = "UTF-8";
		}
		if (prop.getCharset() != null) {
			charsetName = prop.getCharset();
		}
	}

	public void cyclesForever() {
		LOG.debug("Invoke::Start");
		Long currTime = System.currentTimeMillis();
		endTime = currTime + workTime * 1000L;
		ReadLogger readerGf = getInstanceReader();
		goEndFile(readerGf);
		while (isHaveTime()) {
			mainLogicLoop(readerGf);
		}
		LOG.debug("Invoke::End");
	}

	protected void mainLogicLoop(ReadLogger readerGf) {
		try {
			if (readerGf.hasNext()) {
				String logmes = readerGf.getMessage();
				String filteredMessage = filter.getMessageFiltered(logmes);
				LOG.debug("Business data::filteredMessage=" + filteredMessage);
				addAllResult(filteredMessage);
				printMessage(filteredMessage);
			} else {
				sleep();
			}
		} catch (IOException | NotPassedFilter e) {
		}
	}

	protected ReadLogger getInstanceReader() {
		ReadLogger readerGf = null;
		try {
			readerGf = new LoggerGF(filenameLogfile, charsetName);
		} catch (FileNotFoundException | UnsupportedEncodingException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		return readerGf;
	}

	protected void goEndFile(ReadLogger readerGf) {
		if (!prop.isGrepSet()) {
			readerGf.goToEndFile();
		}
	}

	private void sleep() {
		try {
			Thread.sleep(50);
		} catch (InterruptedException e) {
		}
	}

	private void addAllResult(String filteredMessage) {
		LOG.debug("Business data::filteredMessage=" + filteredMessage);
		if (workTime != 0) {
			allResultFiltered.append(filteredMessage + "\n");
		}
	}

	private boolean isHaveTime() {
		Long currTime = System.currentTimeMillis();
		return currTime <= endTime || workTime == 0;
	}

	private void printMessage(String message) {
		String textColorized = messageColorized.getMessageColorized(message, filter.searchWords, prop.isLight());
		viewerOutput(textColorized);
	}

	private void viewerOutput(String text) {
		if (!isSilent) {
			viewer.printMessage(text);
		}
	}

	private void readConfigFile(String filename) {
		configuration.readConfigFile(filename);
	}

	public int getWorkTime() {
		return workTime;
	}

	public void setWorkTime(int workTime) {
		this.workTime = workTime;
	}

	public String getAnalize() {
		return allResultFiltered.toString();
	}

	private void printWords() {
		if (!prop.isSilent()) {
			System.out.println("OR=" + prop.getSearchWords());
			System.out.println("AND=" + prop.getNecessariWords());
			System.out.println("NOT=" + prop.getExceptWords());
		}
	}

	// TODO убрать отсюда, вывод должен быть в другом классе
	private void printUsed() {
		System.out.println("use tailf [-cfg configfile] [-log file] [search word] [^exceptword] [@necessari_word][-grep] [-file] [-A xx] % some text with space %\n");

	}

}