package ru.nsandrus.utils.viewer;

public class Viewer {

	public  void printMessage(String result) {
		System.out.println(result);
		//для server.log удобнее с пустой строкой. но если в searchword пустой, то выводить бы не надо. а то двойная пустая строка идет
		if (result.contains("[#")) {
			System.out.println();
		}
	}
	
	public  void printEmptyLine() {
			System.out.println();
	}

}
