package ru.nsandrus.utils;

import org.apache.log4j.Logger;

public class ParsingArgs {

	final static Logger LOG = Logger.getLogger(ParsingArgs.class);
	private String[] arguments;
	Properties prop = new Properties();

	public ParsingArgs(String[] args) {
		this.arguments = args;
		parsingArgs();
	}

	public void parsingArgs() {



		boolean startWords = false;
		String word = "";
		boolean simbol_need = false;
		boolean simbol_except = false;
		for (int i = 0; i < arguments.length; i++) {
			switch (arguments[i]) {
			case "-grep":
				prop.setGrepSet(true);
				break;
			case "-light":
				prop.setLight(true);
				break;
			case "-silent":
				prop.setSilent(true);
				break;
			case "-log":
				prop.setFilenameLogfile(arguments[++i]);
				break;
			case "-cp":
				prop.setCharset(arguments[++i]);
				break;
			case "-maxout":
				prop.setMaxLenMessage(Integer.parseInt(arguments[++i]));
				System.out.println("maximum output line is " + prop.getMaxLenMessage() + " bytes");
				break;
			case "-time":
				prop.setWorkTime(Integer.parseInt(arguments[++i]));
//				System.out.println("work only " + prop.getWorkTime() + " seconds");
				break;
			case "-cfg":
				prop.setConfigFilename(arguments[++i]);
				break;
			case "-A":
				System.out.println("found -A");
				try {
					prop.setNumPostPrint(Integer.parseInt(arguments[++i]));
				} catch (NumberFormatException e) {
					LOG.fatal("Not number format!");
					System.err.println("-A Not number format!");
					System.exit(0);
				}
				break;
			default:
				String argWord = arguments[i];
				if (argWord.contains("^")) {
					simbol_except = true;
					argWord = argWord.substring(1);
				} else if (argWord.contains("@")) {
					simbol_need = true;
					argWord = argWord.substring(1);
				} {

				if (argWord.contains(Properties.SIMBOL_WORDS)) {
					if (startWords) {
						startWords = false;
						word = word.trim();
						addWords(word, simbol_need, simbol_except);
						simbol_need = false;
						simbol_except = false;
						word = "";
						break;
					} else {
						startWords = true;
						break;
					}
				}

				if (startWords) {
					word += argWord + " ";
				} else {
					word = argWord.trim();
					addWords(word, simbol_need, simbol_except);
					simbol_need = false;
					simbol_except = false;
					word = "";
				}
			}
			}
		}
	}


	protected void addWords(String words, boolean simbol_need, boolean simbol_except) {
		if (simbol_need) {
			LOG.info("necessari word=" + words);
			prop.addNecessariWords(words);
			return;
		}
		if (simbol_except) {
			LOG.info("except word=" + words);
			prop.addExceptWords(words);
			return;
		}
		LOG.info("search words=" + words);
		prop.addSearchWords(words);
	}

	public Properties getProperties() {
		return prop;
	}


}