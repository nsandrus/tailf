package ru.nsandrus.utils;

import java.util.ArrayList;
import org.apache.log4j.Logger;

public class Filter {
	static Logger LOG = Logger.getLogger(Filter.class);

	int numPostPrint;

	protected ArrayList<String> exceptWords;
	protected ArrayList<String> searchWords;
	protected ArrayList<String> necessariWords;
	protected int maxLenMessage;

	private int countPostPrint = 0;

	public Filter(Properties prop) {
		this.searchWords = prop.getSearchWords();
		this.necessariWords = prop.getNecessariWords();
		this.exceptWords = prop.getExceptWords();
		this.maxLenMessage = prop.getMaxLenMessage();
		this.numPostPrint = prop.getNumPostPrint();
	}

	public String getMessageFiltered(String message) throws NotPassedFilter {
		if (countPostPrint > 0) {
			if (message.length() > 5) {
				countPostPrint--;
				if (passFilterOld(message)) {
					countPostPrint = numPostPrint;
				}
			}
			return message;
		}

		if (!passFilterOld(message)) {
			LOG.debug("Filter not passed::return empty message");
			throw new NotPassedFilter();
		}
		countPostPrint = numPostPrint;
		message = getMaxOutText(message);

		// addFoundSessionId(message);
		// throw new NotPassedFilter();
		return message;
	}

	protected void addFoundSessionId(String message) {
		String sessionId = getSessiondId(message);
		if (sessionId.length() > 0) {
			// если фильтр пустой (все выводить), то ДОБАВЛЯТЬ НЕ НАДО!!!
			// добавить или в фильтр OR, но по идее на время надо. либо
			// отдельный фильтр sessionId делать и проверку по нему еще
			// прогонять
			if (!searchWords.contains(sessionId) && searchWords.size() > 0) {
				searchWords.add(sessionId);
			}
		}
	}

	private String getSessiondId(String message) {
		// example of SessionId=192.168.168.113:-507ed233:150ccf3c836:762b
		// SessionId=192.168.168.113:-507ed233:150cd61f347:-5b30
		// SessionID=192.168.168.113:-507ed233:150cd61f347:-432a
		// SessionId=null:
		int startIndex = message.indexOf("SessionId=");
		if (startIndex == -1) {
			startIndex = message.indexOf("SessionID=");
			if (startIndex == -1) {
				return "";
			}
		}
		if (message.contains("SessionId=null") || message.contains("SessionID=null")) {
			return "";
		}
		// System.out.println("Found SessionId=");
		int tempIndex = message.indexOf(":", startIndex);
		tempIndex = message.indexOf(":", tempIndex + 1);
		tempIndex = message.indexOf(":", tempIndex + 1);
		// System.out.println("->" + message.charAt(tempIndex));
		char ch = message.charAt(tempIndex + 1);
		int endIndex;
		if (ch == '-') {
			endIndex = tempIndex + 6;
		} else {
			endIndex = tempIndex + 5;
		}

		String sessionId = message.substring(startIndex, endIndex);
		// System.out.println("--->" + sessionId + "\n"+ message);
		return sessionId;
	}

	protected String getMaxOutText(String message) {
		String result = message;
		if (message.length() > maxLenMessage) {
			result = message.substring(0, maxLenMessage);
			result += "... Crop by maxout to " + maxLenMessage + " bytes";
		}
		return result;
	}

	protected boolean passFilterOld(String message) {
		LOG.debug("Invoke::Start::Business data::message=" + message);
		message = getTruncMaxLength(message);
		LOG.trace("After truncated::Business data::message=" + message);
		String passedText = getFindExceptedText(message);
		LOG.trace("Excepted done::Business data::passedText=" + passedText);
		passedText += getFindNecessariText(message);
		LOG.trace("Necessati done::Business data::passedText=" + passedText);
		passedText += getFindSearchedText(message);
		LOG.trace("Search done::Business data::passedText=" + passedText);
		if (passedText.equals("")) {
			return true;
		}
		LOG.trace("Filtering message::Pass failed::Reason=" + passedText);
		return false;
	}

	protected String getTruncMaxLength(String message) {
		int maxLength = maxLenMessage;
		if (message.length() > maxLength) {
			message = message.substring(0, maxLength);
		}
		return message;
	}

	protected String getFindSearchedText(String message) {
		if (searchWords == null) {
			return "";
		}
		if (searchWords.size() < 1) {
			return "";
		}
		String notfound = "";
		for (String searchText : searchWords) {
			if (message.contains(searchText)) {
				return "";
			}
			notfound += searchText;
		}
		return notfound;
	}

	protected String getFindNecessariText(String message) {
		if (necessariWords != null) {
			for (String text : necessariWords) {
				if (!message.contains(text)) {
					return text;
				}
			}
		}
		return "";
	}

	protected String getFindExceptedText(String message) {
		if (exceptWords != null) {
			for (String text : exceptWords) {
				if (message.contains(text)) {
					return text;
				}
			}
		}
		return "";
	}

}