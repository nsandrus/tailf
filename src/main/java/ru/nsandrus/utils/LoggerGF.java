package ru.nsandrus.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

public class LoggerGF implements ReadLogger {

	private static final String NEW_LINE = "\r\n";
	private static final String END_GF_LINE = "#]";
	private static final String BEGIN_GF_LINE = "[#";
	private static final int MAX_LENGTH_ONE_MESSAGE_GF = 1_000_000;
//	final static Logger LOG = Logger.getLogger(LoggerGF.class);
	private BufferedReader in;
	private long lastSize;
	private File file;
	private String filename;
	private FileInputStream fis;

	public LoggerGF(String filenameIn) throws FileNotFoundException {
		this.filename = filenameIn;
		file = new File(filename);
		fis = new FileInputStream(filename);
		in = new BufferedReader(new InputStreamReader(fis));
	}

	public LoggerGF(String filenameIn, String charsetName) throws FileNotFoundException, UnsupportedEncodingException {
		this.filename = filenameIn;
		file = new File(filename);
		fis = new FileInputStream(filename);
		in = new BufferedReader(new InputStreamReader(fis, charsetName));
	}

	@Override
	public String getMessage() throws IOException {
		String line = in.readLine();
		if (line == null) {
			return "";
		}
		if (line.contains(BEGIN_GF_LINE) && !line.contains(END_GF_LINE)) {
			line = getGlassFishMessage(line);
		}
		return line;
	}

	private String getGlassFishMessage(String line) throws IOException {
		StringBuilder message = new StringBuilder(line);
		checkRollingFile();
		while (in.ready()) {
			message.append(NEW_LINE + in.readLine());
			if (message.indexOf(END_GF_LINE) >= 0 ) {
				break;
			}
			if (message.length() >= MAX_LENGTH_ONE_MESSAGE_GF) {
				System.err.println("MAX length");
				break;
			}
		}
		return message.toString();
	}

	@Override
	public boolean hasNext() throws IOException {
		boolean isReady = in.ready();
		if (!isReady) {
			checkRollingFile();
		}
		return isReady;
	}

	protected void checkRollingFile() throws IOException, FileNotFoundException {
		long newSize = file.length();
		if (newSize < lastSize) {
			System.out.println("Rolling file");
				sleep();
				long newSize2 = file.length();
					if (newSize2 < lastSize) {
						in.close();
						in = new BufferedReader(new FileReader(filename));
					}
		}
		lastSize = newSize;
	}

	protected void sleep() {
		try {
			Thread.sleep(1400);
		} catch (InterruptedException e) {
		}
	}

	@Override
	public void close() throws IOException {
		in.close();
	}

	@Override
	public void goToEndFile() {
		int byteAvialable;
		try {
			byteAvialable = fis.available();
			fis.skip(byteAvialable);
		} catch (IOException e1) {
		}
	}

}
