package ru.nsandrus.utils;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

public class Config {

	final static Logger LOG = Logger.getLogger(Config.class);
	
	private boolean isSilent = false;
	private String configTextData ="";
	private static final String DEFAULT_FILENAME_CFG = "tailf.cfg";
	
	public String readConfigFile(String filename) {
		LOG.debug("Invoke::Start::Filename=" + filename);
		filename = getConfigFilename(filename);
		File file = new File(filename);
		try {
			configTextData = FileUtils.readFileToString(file);
			return configTextData;
		} catch (IOException e) {
			LOG.error("Invoke::End::Error read config file::Filename=" + filename);
			if (!isSilent) {
				LOG.debug("Warning: " + e.getLocalizedMessage());
				LOG.debug("Used default colors");
			}
		}
		return "";
	}
	
	protected String getConfigFilename(String filename) {
		if (filename.isEmpty()) {
			filename = DEFAULT_FILENAME_CFG;
		}
		return filename;
	}

	String getConfigText() {
		if (configTextData.length() == 0) {
			LOG.warn("Invoke::Empty config file may be need read first");
		}
		return configTextData;
	}
}
