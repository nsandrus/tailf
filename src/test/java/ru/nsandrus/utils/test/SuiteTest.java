package ru.nsandrus.utils.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({ ParsingArgsTest.class, MessageTest.class })
public class SuiteTest {

}
