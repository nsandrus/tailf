package ru.nsandrus.utils.test;

import static org.junit.Assert.assertEquals;

import org.junit.Ignore;
import org.junit.Test;

import ru.nsandrus.utils.ParsingArgs;

public class ParsingArgsTest {


	@Test
	public void initialization1(){
		String[] args = {"-log","server.log"};
		ParsingArgs parsing = new ParsingArgs(args);
		assertEquals("Chunk simbol=%::grep=false::silent=false::time=0::A=0::maxout=10000000::cfg=::log=server.log::charset=null::NOT=[]::OR=[]::AND[]", parsing.getProperties().toString());
		}

	@Test
	public void initialization2(){
		String[] args = {"-log","server.log","-A 45","-silent","-cfg","cfgfile"};
		ParsingArgs parsing = new ParsingArgs(args);
		assertEquals("Chunk simbol=%::grep=false::silent=true::time=0::A=0::maxout=10000000::cfg=cfgfile::log=server.log::charset=null::NOT=[]::OR=[-A 45]::AND[]", parsing.getProperties().toString());
		}

	@Test
	public void initialization3(){
		String[] args = {"-log","server.log","-A","45","-cfg","cfgfile","-time","5","-grep","searchword1","% search text %"};
		ParsingArgs parsing = new ParsingArgs(args);
		assertEquals("Chunk simbol=%::grep=true::silent=false::time=5::A=45::maxout=10000000::cfg=cfgfile::log=server.log::charset=null::NOT=[]::OR=[searchword1]::AND[]", parsing.getProperties().toString());
		}

	@Test
	public void initialization4(){
		String[] args = {"-log","server.log","-A","45","-cfg","cfgfile","-time","5","-grep","searchword1","%","search","text","%"};
		ParsingArgs parsing = new ParsingArgs(args);
		assertEquals("Chunk simbol=%::grep=true::silent=false::time=5::A=45::maxout=10000000::cfg=cfgfile::log=server.log::charset=null::NOT=[]::OR=[searchword1, search text]::AND[]", parsing.getProperties().toString());
		}

	@Test
	public void initialization5(){
		String[] args = {"-log","server.log","^dontNeedWord","@NEEDED","^%","DONT","NEED","TEXT","%","-time","5","-grep","searchword1","%","search","text","%"};
		ParsingArgs parsing = new ParsingArgs(args);
		assertEquals("Chunk simbol=%::grep=true::silent=false::time=5::A=0::maxout=10000000::cfg=::log=server.log::charset=null::NOT=[dontNeedWord, DONT NEED TEXT]::OR=[searchword1, search text]::AND[NEEDED]", parsing.getProperties().toString());
		}

	@Test
	public void initialization6(){
		String[] args = {"@NeedWord","@%","NEED","TEXT","%","-maxout","105"};
		ParsingArgs parsing = new ParsingArgs(args);
		assertEquals("Chunk simbol=%::grep=false::silent=false::time=0::A=0::maxout=105::cfg=::log=::charset=null::NOT=[]::OR=[]::AND[NeedWord, NEED TEXT]", parsing.getProperties().toString());
		}

	@Test
	public void initialization7(){
		String[] args = {"@NeedWord","@%","NEED","TEXT","%","-maxout","105","-cp","UTF-8"};
		ParsingArgs parsing = new ParsingArgs(args);
		assertEquals("Chunk simbol=%::grep=false::silent=false::time=0::A=0::maxout=105::cfg=::log=::charset=UTF-8::NOT=[]::OR=[]::AND[NeedWord, NEED TEXT]", parsing.getProperties().toString());
		}

}
