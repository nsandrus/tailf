package ru.nsandrus.utils.test;

import static org.junit.Assert.*;

import java.util.EnumMap;

import org.junit.Test;

import ru.nsandrus.utils.Message;
import ru.nsandrus.utils.Message.COLUMN;


public class MessageTest {
	
	
	

	@Test
	public void testGF1() {
		String readText = "[#|2015-09-30T19:33:06.356+0300|INFO|sun-appserver2.1|sun-database-binding.org.glassfish.openesb.databasebc.OutboundMessageProcessor|_ThreadID=161;_ThreadName=pool-8-thread-4;|Finished processing outbound messages.|#]";
		Message mes = new Message(readText);
		EnumMap<COLUMN, String> map = mes.getMessageMap();
		assertEquals("[#", map.get(COLUMN.START));
		assertEquals("2015-09-30T19:33:06.356+0300", map.get(COLUMN.TIME));
		assertEquals("INFO", map.get(COLUMN.LEVEL));
		assertEquals("sun-appserver2.1", map.get(COLUMN.SERVER));
		assertEquals("sun-database-binding.org.glassfish.openesb.databasebc.OutboundMessageProcessor", map.get(COLUMN.ENGINE));
		assertEquals("_ThreadID=161;_ThreadName=pool-8-thread-4;", map.get(COLUMN.THREAD));
		assertEquals("Finished processing outbound messages.", map.get(COLUMN.DATA));
		assertEquals("#]", map.get(COLUMN.END));
	}
	
	@Test
	public void testKaraf1() {
		String readText = "2015-10-07 09:49:18,159 |  | INFO  | xf/m2s/medallia/ | LoggingOutInterceptor            | eptor.AbstractLoggingInterceptor  239 | 125 - org.apache.cxf.cxf-api - 2.7.11 | Outbound Message";
		Message mes = new Message(readText);
		EnumMap<COLUMN, String> map = mes.getMessageMap();
		assertEquals(readText, map.get(COLUMN.DATA));
		assertEquals("", map.get(COLUMN.START));
	}
	
	@Test
	public void testGFToString() {
		String readText = "[#|2015-09-30T19:33:06.356+0300|INFO|sun-appserver2.1|sun-database-binding.org.glassfish.openesb.databasebc.OutboundMessageProcessor|_ThreadID=161;_ThreadName=pool-8-thread-4;|Finished processing outbound messages.|#]";
		Message mes = new Message(readText);
		assertEquals(readText, mes.toString());
	}
	
	@Test
	public void testKarafToString() {
		String readText = "2015-10-07 09:49:18,159 |  | INFO  | xf/m2s/medallia/ | LoggingOutInterceptor            | eptor.AbstractLoggingInterceptor  239 | 125 - org.apache.cxf.cxf-api - 2.7.11 | Outbound Message";
		Message mes = new Message(readText);
		assertEquals(readText, mes.toString());
	}

	@Test
	public void testKaraf2() {
		String readText = "[#|2015-09-30T19:33:06.356+0300|INFO|sun-appserver2.1|sun-database-binding.org.glassfish.openesb.databasebc.OutboundMessageProcessor|_ThreadID=161;_ThreadName=pool-8-thread-4;|Finished processing| d | outbound messages.|#]";
		Message mes = new Message(readText);
		EnumMap<COLUMN, String> map = mes.getMessageMap();
		assertEquals(readText, map.get(COLUMN.DATA));
		assertEquals("", map.get(COLUMN.START));
	}
	
}
