# TAILF #

This project was build for view log file from GlassFish in real time with filtering and colorized.

### What is this repository for? ###

* For view logs in real time and colorized.
* Message from GlassFish between [# #] like one line.
* Some arguments for filtering.

### How do I get set up? ###

* tailf -log serve.log 

### Contribution guidelines ###

* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin